//
//  ViewController.h
//  stopwatch
//
//  Created by Click Labs 105 on 10/13/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    UILabel *lbl;
    NSTimer *stopTimer;
    NSDate *startDate;
    BOOL running;
}

@property (strong,nonatomic) IBOutlet UILabel *lbl;

-(IBAction)startPressed:(id)sender;
-(IBAction)resetPressed:(id)sender;

-(void)updateTimer;

@end

