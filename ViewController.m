//
//  ViewController.m
//  stopwatch
//
//  Created by Click Labs 105 on 10/13/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *label;




@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;



@end

@implementation ViewController
@synthesize lbl;

- (void)viewDidLoad {

    
    
    
   
    
    
    [super viewDidLoad];
    lbl.text = @"00.00.00";
    running = FALSE;
    startDate = [NSDate date];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)startPressed:(id)sender{
    if(!running){
        running = TRUE;
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
        if (stopTimer == nil) {
            stopTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/10.0
                                                         target:self
                                                       selector:@selector(updateTimer)
                                                       userInfo:nil
                                                        repeats:YES];
        }
    }else{
        running = FALSE;
        [sender setTitle:@"Start" forState:UIControlStateNormal];
        [stopTimer invalidate];
        stopTimer = nil;
    }
}
-(void)updateTimer{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    NSString *timeString=[dateFormatter stringFromDate:timerDate];
    lbl.text = timeString;
    
    
    

}
-(IBAction)resetPressed:(id)sender{
    [stopTimer invalidate];
    stopTimer = nil;
    startDate = [NSDate date];
    lbl.text = @"00.00.00";
    running = FALSE;
}

@end
